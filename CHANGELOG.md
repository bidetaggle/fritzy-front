# CHANGELOG

<!--- next entry here -->

## 0.6.0
2019-11-03

### Features

- **crypto-currency-asset:** add format (df170726b86536b198c3607692eceeb3e04018e8)

## 0.5.0
2019-10-29

### Features

- add cryptocurrencies asset (e4827c4b20ce3d974f1b5c92a024e068a0ea0f92)

## 0.4.0
2019-10-19

### Features

- **template:** add a menu wealth (fcdc3e22482230ec3f5272834bd0138134d87aca)

## 0.3.0
2019-10-18

### Features

- **user:** add login with user/password (b4e17d6f0d6f1df93d57e3cf95cec8771137442d)
- **user:** restrict route on authentication status (0999bea2c2a72531959a0740cae8c4239d5b38e1)
- **template:** show left sidebar while user authenticated (7a3bdd511b94f1a49d5dbf6f2a027718e29e1256)

### Fixes

- ember-codemods (42e09a5d3b22416b3336ebba5311b864708f9650)

## 0.2.0
2019-10-08

### Features

- **user:** register an user with login/password (fdebb4166f1b4fd58e509b328295213519db4bce)
- **user:** add feedback when registered (ed538803b371a8f77e43fbf869f796b30f8dc6ca)
- **user:** debounce register action (cba8a5220fd277cb6b8d1c3f67e17e686fa646d0)

## 0.1.1
2019-09-22

### Fixes

- add license (431513c49788da21e43774aee36aa6ac02e11092)

## 0.1.0
2019-09-02

### Features

- add admin-lte-template (248d50ea4614fbb4640ce6e7531152a5324c4857)