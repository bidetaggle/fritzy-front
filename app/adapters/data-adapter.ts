import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import DS from 'ember-data';
import { Session } from 'ember-simple-auth/services/session';

const { JSONAPIAdapter } = DS;

export default class DataAdapter extends JSONAPIAdapter {
    @service session!: Session;

    @computed('session.data.authenticated.token')
    get headers() {
        if (this.session.isAuthenticated && this.session.data) {
            return { Authorization: 'Bearer ' + this.session.data.authenticated.token };
        }

        return {};
    }

    handleResponse(
        status: number,
        headers: object,
        payload: object,
        requestData: object,
    ): object {
        if (status === 401 && this.session.isAuthenticated) {
            this.session.invalidate();
        }
        return super.handleResponse(status, headers, payload, requestData);
    }
}

declare module 'ember-data/types/registries/adapter' {
    export default interface AdapterRegistry {
        'data-adapter': DataAdapter;
    }
}
