import { assign } from '@ember/polyfills';
import LoginUserValidations from './login-user';
import {
    validateConfirmation,
    validateFormat
} from 'ember-changeset-validations/validators';

export const RegisterUserValidations = {
    email: validateFormat({ allowBlank: true, type: 'email' }),
    passwordConfirmation: validateConfirmation({ on: 'password' })
};

export default assign({}, LoginUserValidations, RegisterUserValidations);
