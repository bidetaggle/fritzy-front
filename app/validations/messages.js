export default {
    present: 'errors.validations.front.not-present',
    tooShort: 'errors.validations.front.too-short'
}
