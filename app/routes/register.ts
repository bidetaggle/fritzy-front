import UnauthenticatedRoute from 'fritzy-front/routes/unauthenticated-route';

export default class RegisterRoute extends UnauthenticatedRoute {
    activate() {
        document.body.classList.add('register-page');
    }

    deactivate() {
        document.body.classList.remove('register-page');
    }

}
