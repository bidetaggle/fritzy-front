import { inject as service } from '@ember/service';
import Route from '@ember/routing/route';

export default Route.extend({
    intl: service(),

    async beforeModel() {
        let locale = 'fr-fr';
        const translations = await fetch('/translations/fr-fr.json');
        this.intl.addTranslations(locale, await translations.json());
        this.intl.setLocale(locale);
    },

    renderTemplate(/*controller, model*/) {
        this._super(...arguments);
        document.body.classList.remove('hold-transition');
    }

});
