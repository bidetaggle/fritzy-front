import DS from 'ember-data';
import Model from 'ember-data/model';
import CryptoCurrency from './crypto-currency';

const { attr, belongsTo } = DS;

export default class CryptoCurrencyAddress extends Model {
  @attr() address!: string;
  @attr() amount!: string;
  @attr() description!: string;
  @belongsTo('crypto-currency') cryptoCurrency!: CryptoCurrency;

}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'crypto-currency-address': CryptoCurrencyAddress;
  }
}
