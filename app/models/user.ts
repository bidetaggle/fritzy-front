import DS from 'ember-data';
import Model from 'ember-data/model';
const { attr } = DS;

export default class User extends Model {
    @attr() email!: string;
    @attr() username!: string;
    @attr() password!: string;

}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
    interface ModelRegistry {
        user: User;
    }
}
