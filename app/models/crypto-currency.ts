const { attr } = DS;
import DS from 'ember-data';
import Model from 'ember-data/model';

export default class CryptoCurrency extends Model {
  @attr() name!: string;
}

// DO NOT DELETE: this is how TypeScript knows how to look up your models.
declare module 'ember-data/types/registries/model' {
  export default interface ModelRegistry {
    'crypto-currency': CryptoCurrency;
  }
}
