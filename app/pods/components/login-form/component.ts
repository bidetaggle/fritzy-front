import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import Changeset from 'ember-changeset';
import lookupValidator from 'ember-changeset-validations';
import { task } from 'ember-concurrency';
import Task from 'ember-concurrency/task';
import Credentials from 'fritzy-front/models/credentials';
import LoginUserValidations from 'fritzy-front/validations/login-user';

interface LoginFormArgs {
    onLogin: (credentials: Credentials) => void;
}

export default class LoginForm extends Component<LoginFormArgs> {
    credentials: Credentials = new Credentials();
    changeset: any = new Changeset(this.credentials, lookupValidator(LoginUserValidations), LoginUserValidations);
    @tracked invalidCredentials: boolean = false;
    @service session!: any;

    get isLoginButtonDisabled() {
        return this.changeset.isInvalid || !this.changeset.change.username || !this.changeset.change.password;
    }

    @(task(function*(this: LoginForm) {
        try {
            this.changeset.execute();
            yield this.args.onLogin(this.changeset.data);
        } catch (e) {
            if (e.status === 401) {
                this.credentials.username = '';
                this.credentials.password = '';
                this.changeset.rollback();
                this.invalidCredentials = true;
            } else {
                throw e;
            }
        }
    }).drop())
    login!: Task;

}
