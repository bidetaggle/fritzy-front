import RouterService from '@ember/routing/router-service';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import Changeset from 'ember-changeset';
import { task } from 'ember-concurrency';
import Task from 'ember-concurrency/task';
import { DS } from 'ember-data';
import Store from 'ember-data/store';
import CryptoCurrency from 'fritzy-front/models/crypto-currency';
import CryptoCurrencyAddress from 'fritzy-front/models/crypto-currency-address';

interface AssetsCryptoCurrenciesAddArgs {
    onCancel: () => void;
    onSuccessfullAdd: () => void;
}

export default class AssetsCryptoCurrenciesAdd extends Component<AssetsCryptoCurrenciesAddArgs> {
    @service store!: Store;
    @service router!: RouterService;

    cryptoCurrencyAddress: CryptoCurrencyAddress = this.store.createRecord('crypto-currency-address');
    changeset: any = new Changeset(this.cryptoCurrencyAddress);
    cryptoCurrencies: DS.PromiseArray<CryptoCurrency> = this.store.findAll('crypto-currency');

    @(task(function*(this: AssetsCryptoCurrenciesAdd) {
        yield this.changeset.save();
        this.args.onSuccessfullAdd();
    }).drop())
    addAddress!: Task;

    willDestroy(this: AssetsCryptoCurrenciesAdd) {
        if (this.cryptoCurrencyAddress.isNew) {
            this.store.deleteRecord(this.cryptoCurrencyAddress);
        }
    }
}
