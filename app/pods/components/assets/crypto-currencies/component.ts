import { filterBy } from '@ember/object/computed';
import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import { DS } from 'ember-data';
import Store from 'ember-data/store';
import CryptoCurrencyAddress from 'fritzy-front/models/crypto-currency-address';

interface AssetsCryptoCurrenciesArgs {
    onAdd: () => void;
}

export default class AssetsCryptoCurrencies extends Component<AssetsCryptoCurrenciesArgs> {
    @service store!: Store;

    cryptoCurrencyAddresses: DS.PromiseArray<CryptoCurrencyAddress> = this.store.findAll('crypto-currency-address');

    @filterBy('cryptoCurrencyAddresses', 'isNew', false)
    persistedAddresses!: CryptoCurrencyAddress;

}
