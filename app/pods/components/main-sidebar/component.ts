import { inject as service } from '@ember/service';
import Component from '@glimmer/component';
import { Session } from 'ember-simple-auth/services/session';

export default class MainSidebarComponent extends Component {

    @service session!: Session;

    constructor(owner: any, args: any) {
        super(owner, args);
        document.body.classList.add('sidebar-mini');
    }

}
