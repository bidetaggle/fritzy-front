import { action } from '@ember/object';
import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

interface MainSidebarTreeViewArgs {
    icon: string;
    label: string;
}

export default class MainSidebarTreeView extends Component<MainSidebarTreeViewArgs> {
    @tracked isOpen: boolean = false;

    @action
    toggle(event: MouseEvent) {
        event.preventDefault();
        event.stopPropagation();

        this.isOpen = !this.isOpen;
    }
}
