import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class OtherAssetsController extends Controller {
    @service session!: any;

    @tracked addCryptoCurrencyAddressVisible: boolean = false;

    @action
    hideAddCryptoCurrencyAddress() {
        this.addCryptoCurrencyAddressVisible = false;
    }

    @action
    showAddCryptoCurrencyAddress() {
        this.addCryptoCurrencyAddressVisible = true;
    }

}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
    interface Registry {
        'other-assets-controller': OtherAssetsController;
    }
}
