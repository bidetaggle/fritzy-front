import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import Task from 'ember-concurrency/task';
import ENV from 'fritzy-front/config/environment';
import Credentials from 'fritzy-front/models/credentials';

export default class LoginController extends Controller {
    @service session!: any;

    @(task(function*(this: LoginController, credentials: Credentials) {
        yield this.session.authenticate(ENV.APP.authenticator, credentials);
    }).drop())
    login!: Task;

}

// DO NOT DELETE: this is how TypeScript knows how to look up your controllers.
declare module '@ember/controller' {
    interface Registry {
        'login-controller': LoginController;
    }
}
