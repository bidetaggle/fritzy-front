import UnauthenticatedRoute from 'fritzy-front/routes/unauthenticated-route';

export default class LoginRoute extends UnauthenticatedRoute {

  activate() {
    document.body.classList.add('login-page');
  }

  deactivate() {
    document.body.classList.remove('login-page');
  }
}
