import Controller from '@ember/controller';
import { InvalidError } from '@ember-data/adapter/error';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';

export default Controller.extend({
    intl: service(),
    swal: service(),

    register: task(function* (changeset) {
        try {
            let user = yield changeset.save();
            this.swal.open({
                title: this.intl.t('register.registered', { username: user.username }),
                type: 'success'
            });
            this.transitionToRoute('login');
        } catch (e) {
            if (e instanceof InvalidError) {
                changeset.data.errors.forEach(({ attribute, message }) => {
                    changeset.pushErrors(attribute, message);
                });
            } else {
                throw e;
            }
        }

    }).drop()

});
