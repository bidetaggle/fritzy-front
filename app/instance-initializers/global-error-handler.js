import { debug } from '@ember/debug';
import Ember from 'ember';
import { UnauthorizedError } from '@ember-data/adapter/error';

export function initialize(appInstance) {
  const intl = appInstance.lookup('service:intl');
  const swal = appInstance.lookup('service:swal');

  // Global error handler in Ember run loop
  // TODO - catch different type of errors
  Ember.onerror = function (err) {
    if (err instanceof UnauthorizedError) {
      debug('Unauthorized error received :' + err);
    }
    swal.open({
      title: intl.t('global-error'),
      type: 'error'
    });

    throw err;
  };
}

export default {
  name: 'global-error-handler',
  initialize
};
