import ApplicationInstance from '@ember/application/instance';

export function initialize(appInstance: ApplicationInstance): void {
  const applicationRoute = appInstance.lookup('route:application');
  const session = appInstance.lookup('service:session');

  session.on('authenticationSucceeded', () => {
    applicationRoute.transitionTo('user');
  });

  session.on('invalidationSucceeded', () => {
    applicationRoute.transitionTo('login');
  });
}

export default {
  initialize,
  name: 'session-events',
  after: 'ember-simple-auth',
};
