import { helper } from '@ember/component/helper';

export function formatCryptoCurrency(params: [number]) {
  const value = params[0];
  const tokens = Math.floor(value / 100000000);
  let cents = (value % 100000000).toString();
  cents = cents.padStart(8, '0');

  return tokens + '.' + cents;
}

export default helper(formatCryptoCurrency);
