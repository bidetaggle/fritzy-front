import Changeset from 'ember-changeset';
import Component from '@ember/component';
import { computed } from '@ember/object';
import lookupValidator from 'ember-changeset-validations';
import { inject as service } from '@ember/service';
import UserValidations from '../validations/register-user';

export default Component.extend({
    store: service(),

    init() {
        this._super(...arguments);
        let model = this.store.createRecord('user');
        this.changeset = new Changeset(model, lookupValidator(UserValidations), UserValidations);
    },

    isRegisterButtonDisabled: computed('changeset.isInvalid', function () {
        return this.get('changeset.isInvalid') || !this.get('changeset.change.username') || !this.get('changeset.change.password');
    }),

    actions: {
        erasePasswordConfirmation() {
            let changeset = this.changeset;
            if (this.get('changeset.change.passwordConfirmation')) {
                changeset.set('passwordConfirmation', '');
            }
        }
    }

});
