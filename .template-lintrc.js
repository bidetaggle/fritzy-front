'use strict';

module.exports = {
  extends: 'recommended',

  rules: {
    'no-bare-strings': false, // TODO: to activate when translations is done
  }
};
