import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';
import setupSinon from 'ember-sinon-qunit';
import Application from 'fritzy-front/app';
import 'qunit-dom'; // assert.dom type
import config from '../config/environment';

setApplication(Application.create(config.APP));

setupSinon();

start();
