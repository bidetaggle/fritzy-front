import { setupTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Unit | Route | user/other-assets', hooks => {
  setupTest(hooks);

  test('it exists', function(assert) {
    const route = this.owner.lookup('route:user/other-assets');
    assert.ok(route);
  });
});
