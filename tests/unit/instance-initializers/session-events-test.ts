import Application from '@ember/application';
import EmberRouter from '@ember/routing/router';
import { run } from '@ember/runloop';
import Session from 'ember-simple-auth/services/session';
import InstanceInitializerTestContext from 'fritzy-front/instance-initializers-tests';
import { initialize } from 'fritzy-front/instance-initializers/session-events';
import { module, test } from 'qunit';
import sinon from 'sinon';

module('Unit | Instance Initializer | session-events', hooks => {

  hooks.beforeEach(function(this: InstanceInitializerTestContext) {
    this.TestApplication = Application.extend();
    this.TestApplication.instanceInitializer({
      name: 'initializer under test',
      initialize,
    });

    this.application = this.TestApplication.create({ autoboot: false });
    this.application.register('route:application', EmberRouter);
    this.application.register('service:session', Session);

    this.instance = this.application.buildInstance();
  });

  hooks.afterEach(function(this: InstanceInitializerTestContext) {
    run(this.instance, 'destroy');
    run(this.application, 'destroy');
  });

  test('it redirects on user on authenticationSucceeded', async function(this: InstanceInitializerTestContext, assert) {
    // Given
    await this.instance.boot();
    const applicationRoute = this.instance.lookup('route:application');
    const applicationRouteStub = sinon.stub(applicationRoute, 'transitionTo');
    const sessionService = this.instance.lookup('service:session');

    // When
    sessionService.trigger('authenticationSucceeded');

    // Then
    assert.ok(applicationRouteStub.called, 'EmberRouter.transitionTo should be called');
    assert.ok(applicationRouteStub.getCalls()[0].args[0] === 'user', 'Should transition to index route');
  });

  test('it redirects on login on invalidationSucceeded', async function(this: InstanceInitializerTestContext, assert) {
    // Given
    await this.instance.boot();
    const applicationRoute = this.instance.lookup('route:application');
    const applicationRouteStub = sinon.stub(applicationRoute, 'transitionTo');
    const sessionService = this.instance.lookup('service:session');

    // When
    sessionService.trigger('invalidationSucceeded');

    // Then
    assert.ok(applicationRouteStub.called, 'EmberRouter.transitionTo should be called');
    assert.ok(applicationRouteStub.getCalls()[0].args[0] === 'login', 'Should transition to index route');
  });

});
