import { setupTest } from 'ember-qunit';
import { formatCryptoCurrency } from 'fritzy-front/helpers/format-crypto-currency';
import { module, test } from 'qunit';

module('Unit | Helper | format-crypto-currency', hooks => {
    setupTest(hooks);

    test('it formats', assert => {
        assert.equal(formatCryptoCurrency([0]), '0.00000000');
        assert.equal(formatCryptoCurrency([1234]), '0.00001234');
        assert.equal(formatCryptoCurrency([12341234]), '0.12341234');
        assert.equal(formatCryptoCurrency([112341234]), '1.12341234');
        assert.equal(formatCryptoCurrency([1012341234]), '10.12341234');
        assert.equal(formatCryptoCurrency([1012341230]), '10.12341230');
    });
});
