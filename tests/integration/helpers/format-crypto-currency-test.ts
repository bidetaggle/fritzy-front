import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Helper | format-crypto-currency', hooks => {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // Given
    this.set('inputValue', '1234');

    // When
    await render(hbs`{{format-crypto-currency inputValue}}`);

    // Then
    assert.equal(this.element.textContent, '0.00001234');
  });
});
