import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import setupMirage from 'ember-cli-mirage/test-support/setup-mirage';
import { setupIntl } from 'ember-intl/test-support';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';
import sinon from 'sinon';

module('Integration | Component | assets/crypto-currencies/add', hooks => {
  setupRenderingTest(hooks);
  setupIntl(hooks);
  setupMirage(hooks);

  test('it renders', async function(assert) {
    this.set('stub', sinon.stub());

    await render(hbs`<Assets::CryptoCurrencies::Add @onCancel={{action stub}} @onSuccessfullAdd={{action stub}}/>`);

    assert.dom('.card').exists();
  });
});
