import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Component | admin-lte/card/card-body', hooks => {
  setupRenderingTest(hooks);

  test('it renders', async assert => {
    await render(hbs`<AdminLte::Card::CardBody />`);

    assert.dom('.card-body').exists();
  });
});
