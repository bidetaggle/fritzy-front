import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Component | admin-lte/layout', hooks => {
  setupRenderingTest(hooks);

  test('it renders', async assert => {
    await render(hbs`{{admin-lte/layout}}`);

    assert.dom('.wrapper').exists();
  });
});
