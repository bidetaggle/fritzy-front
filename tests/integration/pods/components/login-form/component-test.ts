import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupIntl } from 'ember-intl/test-support';
import { setupRenderingTest } from 'ember-qunit';
import Credentials from 'fritzy-front/models/credentials';
import { module, test } from 'qunit';
import sinon from 'sinon';

module('Integration | Component | login-form', hooks => {
  setupRenderingTest(hooks);
  setupIntl(hooks);

  test('it renders', async function(assert) {
    // Given
    this.set('loginAction', sinon.stub());

    // When
    await render(hbs`<LoginForm @onLogin={{action loginAction}} />`);

    // Then
    assert.dom('#login-form-submit').exists('Login form submit button not found!');
  });

  test('it makes user logged', async function(assert) {
    // Given
    const loginAction = sinon.spy();
    this.set('loginAction', loginAction);
    await render(hbs`<LoginForm @onLogin={{action loginAction}} />`);

    await fillIn('#login-form-input-username', 'oliver');
    await fillIn('#login-form-input-password', 'password');

    // When
    await click('#login-form-submit');

    // Then
    assert.ok(loginAction.called, 'Login action shoud be called');
    assert.ok(loginAction.getCalls()[0].args[0] instanceof Credentials, 'A credentials should be pass at the login action');
  });
});
