import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { setupRenderingTest } from 'ember-qunit';
import { module, test } from 'qunit';

module('Integration | Component | main-sidebar/tree-view', hooks => {
  setupRenderingTest(hooks);

  test('it renders', async function(assert) {
    // When
    await render(hbs`<MainSidebar::TreeView @label="treeviewmenu"/>`);

    // Then
    assert.dom(this.element).containsText('treeviewmenu');
  });

  test('it renders with block usage', async function(assert) {
    // When
    await render(hbs`
      <MainSidebar::TreeView @label="treeviewmenu">
        template block text
      </MainSidebar::TreeView>
    `);

    // Then
    assert.dom(this.element).containsText('template block text');
  });
});
