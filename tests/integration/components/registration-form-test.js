import { hbs } from 'ember-cli-htmlbars';
import { module, test } from 'qunit';
import { click, fillIn, render } from '@ember/test-helpers';
import { setupIntl } from 'ember-intl/test-support';
import { setupMirage } from 'ember-cli-mirage/test-support';
import { setupRenderingTest } from 'ember-qunit';
import sinon from 'sinon';
import isChangeset from 'ember-changeset/utils/is-changeset';

module('Integration | Component | registration-form', function (hooks) {
  setupRenderingTest(hooks);
  setupIntl(hooks);
  setupMirage(hooks);

  test('it renders', async function (assert) {
    // Given
    this.set('registerAction', sinon.stub());

    // When
    await render(hbs`<RegistrationForm @onRegister={{action registerAction}} />`);

    // Then
    assert.dom('#registration-form-submit').exists('Registration form submit button not found!');
  });

  test('it register user', async function (assert) {
    // Given
    const registerAction = sinon.spy();
    this.set('registerAction', registerAction);
    await render(hbs`<RegistrationForm @onRegister={{action registerAction}} />`);

    await fillIn('#registration-form-input-username', 'oliver');
    await fillIn('#registration-form-input-password', 'password');
    await fillIn('#registration-form-input-passwordConfirmation', 'password');

    // When
    await click('#registration-form-submit');

    // Then
    assert.ok(registerAction.called, 'Register action shoud be called');
    assert.ok(isChangeset(registerAction.getCalls()[0].args[0]), 'A changeset should be pass at the register action');
  });

});
