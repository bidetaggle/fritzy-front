import { hbs } from 'ember-cli-htmlbars';
import { module, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { render } from '@ember/test-helpers';

module('Integration | Component | main-sidebar', function (hooks) {
  setupRenderingTest(hooks);

  skip('it renders', async function (assert) {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<MainSidebar />`);

    assert.dom(this.element).hasText('');

    // Template block usage:
    // await render(hbs`
    //   <MainSidebar>
    //     template block text
    //   </MainSidebar>
    // `);

    // assert.equal(this.element.textContent.trim(), 'template block text');
  });
});
