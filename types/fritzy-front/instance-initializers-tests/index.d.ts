declare module 'fritzy-front/instance-initializers-tests' {
    import Application from '@ember/application';
    import ApplicationInstance from '@ember/application/instance';
    import { TestContext } from 'ember-test-helpers';


    export default interface InstanceInitializerTestContext extends TestContext {
        TestApplication: any;
        application: Application;
        instance: ApplicationInstance;
    }
}
