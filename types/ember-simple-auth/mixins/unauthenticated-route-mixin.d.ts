declare module 'ember-simple-auth/mixins/unauthenticated-route-mixin' {
    import Mixin from '@ember/object/mixin';

    interface UnauthenticatedRoute {
        routeIfAlreadyAuthenticated: string;
    }

    const UnauthenticatedRouteMixin: Mixin<UnauthenticatedRoute>;

    export default UnauthenticatedRouteMixin;
}
