declare module 'ember-simple-auth/mixins/authenticated-route-mixin' {
    import Mixin from '@ember/object/mixin';

    interface AuthenticatedRoute {
        authenticationRoute: string;
    }

    const AuthenticatedRouteMixin: Mixin<AuthenticatedRoute>;

    export default AuthenticatedRouteMixin;
}
