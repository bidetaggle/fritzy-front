import { ValidatorFunc } from 'ember-changeset/types';

export default function lookupValidator(validationMap: { [s: string]: ValidatorFunc | ValidatorFunc[] }): ValidatorFunc;
