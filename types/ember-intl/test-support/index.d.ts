export function setupIntl(hooks: NestedHooks, locale?: string | string[], translations?: any): void;
