import { Response } from '@miragejs/server';
import cryptoCurrencyAddresses from './crypto-currency-addresses';

const createFakeToken = obj => {
  return 'a.' + btoa(JSON.stringify(obj)) + '.b';
};

const getConvertedTime = time => {
  return time / 1000;
};

export default function () {
  this.passthrough();
  this.timing = 2000;

  // Authentication
  this.post('/authentication/credentials', function (db, request) {
    const params = JSON.parse(request.requestBody);
    if (params.username === "oliver" && params.password === "password") {
      const currentTime = getConvertedTime(Date.now());
      const expiresAt = currentTime + 60;
      const tokenData = {
        'expiresAt': expiresAt
      };
      const token = createFakeToken(tokenData);
      const response = {
        'token': token
      };
      return new Response(200, { 'Content-Type': 'application/json' }, JSON.stringify(response));
    } else {
      const body = { errors: 'Invalid credentials' };
      return new Response(401, {}, body);
    }
  });

  // Crypto currencies
  this.get('/crypto-currencies', (schema) => {
    return schema.cryptoCurrencies.all();
  });

  this.post('/crypto-currencies');

  // Users
  this.get('/users', (schema) => {
    return schema.users.all();
  });

  this.post('/users');
  cryptoCurrencyAddresses(this);
}
