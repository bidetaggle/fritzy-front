import { Server } from 'ember-cli-mirage';

export default function(mirage: Server) {
    mirage.get('/crypto-currency-addresses');
    mirage.post('/crypto-currency-addresses');
}
